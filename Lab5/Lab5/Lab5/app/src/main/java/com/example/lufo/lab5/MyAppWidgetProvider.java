package com.example.lufo.lab5;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RemoteViews;

public class MyAppWidgetProvider extends AppWidgetProvider {
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        final int N = appWidgetIds.length;
        for (int i = 0; i < N; i++) {
            int appWidgetId = appWidgetIds[i];
            Intent clickIntent = new Intent(context, List.class);
            PendingIntent myPendingIntent = PendingIntent.getActivity(context, 0, clickIntent, 0);
            RemoteViews myRemoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
            myRemoteViews.setOnClickPendingIntent(R.id.widget_fruit_image, myPendingIntent);
            appWidgetManager.updateAppWidget(appWidgetId, myRemoteViews);
        }
    }

    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (intent.getAction().equals(context.getResources().getString(R.string.broadcast_name))) {
            RemoteViews myRemoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
            Bundle bundle = intent.getExtras();
            myRemoteViews.setTextViewText(R.id.widget_fruit_name, bundle.getString("name"));
            myRemoteViews.setImageViewResource(R.id.widget_fruit_image, bundle.getInt("image"));
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            appWidgetManager.updateAppWidget(new ComponentName(context, MyAppWidgetProvider.class), myRemoteViews);
        }
    }
}