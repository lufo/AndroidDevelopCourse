package com.example.lufo.lab6;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SimpleAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MyActivity extends Activity {

    private String currPath = "/storage/emulated/0/Music/1.mp3";
    private int currPosition = 0;
    private Button start;
    private Button next;
    private Button past;
    private ListView list;
    private SeekBar mySeekBar;
    private Handler myHandler = new Handler();
    private MusicPlayerService myService;
    private ArrayList<Map<String, Object>> myDataList = new ArrayList<Map<String, Object>>();
    private Runnable r = new Runnable() {
        @Override
        public void run() {

            if (mySeekBar.getMax() <= 100) {
                //Log.v("666", ((Integer) mySeekBar.getMax()).toString());
                mySeekBar.setMax(myService.getDuration());
            }
            //Log.v("777777", ((Integer) (myService.getCurrentPosition())).toString());

            mySeekBar.setProgress(myService.getCurrentPosition());

            //mySeekBar.setProgress((int) (myService.getCurrentPosition() * 100.0 / myService.getDuration()));

            myHandler.postDelayed(this, 1000);
        }
    };

    private ServiceConnection sc = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            myService = ((MusicPlayerService.SMPlayerBinder) iBinder).getService();
            myHandler.post(r);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            myService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        start = (Button) findViewById(R.id.start);
        past = (Button) findViewById(R.id.past);
        next = (Button) findViewById(R.id.next);
        mySeekBar = (SeekBar) findViewById(R.id.seek_bar);
        list = (ListView) findViewById(R.id.music_list);

        Intent myIntent = new Intent(this, MusicPlayerService.class);
        Intent bindIntent = new Intent(this, MusicPlayerService.class);
        startService(myIntent);
        bindService(bindIntent, sc, BIND_AUTO_CREATE);
/*
        try {
            myPlayer.setDataSource("/storage/emulated/0/Music/1.mp3");
            myPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            myPlayer.prepare();
        } catch (IOException e) {
        }
*/

        generateListView();


        mySeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (b) {
                    myService.seekTo(i);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myService.playOrPause(currPath);
                //Log.v("666666", ((Integer) (myService.getDuration())).toString());
                mySeekBar.setMax(myService.getDuration());
                //mySeekBar.setProgress(0);
            }
        });

        past.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currPosition == 0)
                    currPosition = myDataList.size() - 1;
                else
                    currPosition--;
                currPath = myDataList.get(currPosition).get("path").toString();
                myService.playOrPause(currPath);
                mySeekBar.setMax(myService.getDuration());
                mySeekBar.setProgress(0);
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currPosition == myDataList.size() - 1)
                    currPosition = 0;
                else
                    currPosition++;
                currPath = myDataList.get(currPosition).get("path").toString();
                myService.playOrPause(currPath);
                mySeekBar.setMax(myService.getDuration());
                mySeekBar.setProgress(0);
            }
        });
    }

    private void generateListView() {
        List<File> myList = new ArrayList<File>();
        int count = 1;
        findAll("/storage/emulated/0/Music", myList);
        Collections.sort(myList);
        for (File file : myList) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("name", count + "_" + file.getName());
            map.put("path", file.getAbsolutePath());
            myDataList.add(map);
            count++;
        }
        SimpleAdapter listItemAdapter = new SimpleAdapter(this, myDataList, R.layout.item,
                new String[]{"name"}, new int[]{R.id.music_name});

        list.setAdapter(listItemAdapter);
        list.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {
                currPosition = position;
                String path = myDataList.get(currPosition).get("path").toString();
                currPath = path;
                myService.playOrPause(currPath);
                mySeekBar.setMax(myService.getDuration());
                mySeekBar.setProgress(0);
            }
        });
    }

    private void findAll(String path, List<File> myList) {
        File myFile = new File(path);
        File[] subFiles = myFile.listFiles();
        if (subFiles != null)
            for (File subFile : subFiles) {
                if (subFile.isFile() && subFile.getName().endsWith(".mp3"))
                    myList.add(subFile);
            }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.NONE, 0, Menu.NONE, "quit");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                AlertDialog.Builder builder = new AlertDialog.Builder(MyActivity.this);
                builder.setMessage("quit");
                builder.setPositiveButton("confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        onDestroy();
                        System.exit(0);
                    }
                });
                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.create().show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*
    @Override
    protected void onResume() {
        if (myService != null)
            mySeekBar.setMax(myService.getDuration());
        super.onResume();
    }
    */

    @Override
    protected void onDestroy() {
        unbindService(sc);
        super.onDestroy();
    }
}