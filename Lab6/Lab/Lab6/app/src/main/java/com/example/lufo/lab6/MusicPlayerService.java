package com.example.lufo.lab6;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;

import java.io.IOException;

/**
 * Created by lufo on 14-11-13.
 */

enum STATE {
    IDLE, STOP, PLAYING, PAUSE
}

public class MusicPlayerService extends Service {
    String currPath = "";
    public final IBinder myBinder = new SMPlayerBinder();
    public MediaPlayer myPlayer = new MediaPlayer();

    STATE state = STATE.IDLE;

    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    public void playOrPause(String filePath) {
        /*myPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                int duration = myPlayer.getDuration();
            }
        });*/

        if (currPath != filePath) {
            if (currPath != "") {
                myPlayer.reset();
                //myPlayer.seekTo(0);
                state = STATE.IDLE;
            }
            currPath = filePath;
        }
        if (state == STATE.IDLE) {
            try {
                //myPlayer.reset();
                myPlayer.setDataSource(filePath);
                myPlayer.prepare();
                myPlayer.start();
                state = STATE.PLAYING;
            } catch (IOException e) {

            }
        } else if (state == STATE.PAUSE) {
            myPlayer.start();
            state = STATE.PLAYING;
        } else {
            myPlayer.pause();
            state = STATE.PAUSE;
        }
    }


    public int getDuration() {
        if (state == STATE.IDLE)
            return 0;
        else
            return myPlayer.getDuration();
    }

    public int getCurrentPosition() {
        if (state == STATE.IDLE)
            return 0;
        else
            return myPlayer.getCurrentPosition();
    }

    /*public void seekTo(int i) {
        myPlayer.seekTo(i * myPlayer.getDuration() / 100);
    }*/

    public void seekTo(int i) {
        myPlayer.seekTo(i);
    }

    @Override
    public void onDestroy() {
        myPlayer.release();
        super.onDestroy();
    }

    public class SMPlayerBinder extends Binder {
        MusicPlayerService getService() {
            return MusicPlayerService.this;
        }
    }
}
