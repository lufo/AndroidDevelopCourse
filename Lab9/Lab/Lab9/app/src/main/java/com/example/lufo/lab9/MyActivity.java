package com.example.lufo.lab9;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class MyActivity extends Activity {

    private TextView qq;
    private Button check;
    private TextView result;
    // private final Handler handler = new Handler();
    private String url = "http://webservice.webxml.com.cn/webservices/qqOnlineWebService.asmx/qqCheckOnline";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        qq = (TextView) findViewById(R.id.qq);
        check = (Button) findViewById(R.id.check);
        result = (TextView) findViewById(R.id.result);

        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Bundle b = msg.getData();
                String state = b.get("state").toString();
                Log.v("2222", state);
                if (state.equals("Y"))
                    result.setText("在线");
                else if (state.equals("N"))
                    result.setText("离线");
                else if (state.equals("E"))
                    result.setText("QQ号码错误");
                else if (state.equals("A"))
                    result.setText("商业用户认证失败");
                else
                    result.setText("免费用户超过流量");
            }
        };

        class DownloadThread implements Runnable {
            private String qqNumber;

            public DownloadThread(String num) {
                qqNumber = num;
            }

            @Override
            public void run() {
                String returnCode = "";
                HttpPost httpPost = new HttpPost(url);
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("qqCode", qqNumber));
                // Log.v("1111", qqNumber);
                try {
                    //Log.v("9999", "9999");
                    httpPost.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                DefaultHttpClient httpClient = new DefaultHttpClient();
                try {
                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity result = response.getEntity();
                    String xml = EntityUtils.toString(result);
                    //Log.v("7777", xml);
                    XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser xpp = factory.newPullParser();
                    xpp.setInput(new StringReader(xml));
                    int eventType = xpp.getEventType();
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.TEXT)
                            returnCode = xpp.getText();
                        eventType = xpp.next();
                    }
                } catch (Exception e) {

                    e.printStackTrace();
                }
                Bundle b = new Bundle();
                b.putString("state", returnCode);
                Message msg = handler.obtainMessage();
                msg.setData(b);
                Log.v("6666", returnCode);
                handler.sendMessage(msg);
                //handler.handleMessage(msg);
            }
        }
        //new Thread(new DownloadThread(qqNumber)).start();

        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String qqNumber = qq.getText().toString();
                //DownloadThread thread = new DownloadThread(qqNumber);
                result.setText("查询中");
                new Thread(new DownloadThread(qqNumber)).start();
                //handler.post(thread);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
