package com.example.lufo.lab8;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by lufo on 14-11-29.
 */


public class AddContact extends Activity {
    private EditText studentID;
    private EditText name;
    private EditText mClass;
    private EditText phoneNumber;
    private Button add;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_contact);

        studentID = (EditText) findViewById(R.id.student_id_input);
        name = (EditText) findViewById(R.id.name_input);
        mClass = (EditText) findViewById(R.id.class_input);
        phoneNumber = (EditText) findViewById(R.id.phone_number_input);
        add = (Button) findViewById(R.id.button_add);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertContacts();
            }
        });
    }

    private void insertContacts() {
        String strStudentID = studentID.getText().toString();
        String strName = name.getText().toString();
        String strClass = mClass.getText().toString();
        String strPhoneNumber = phoneNumber.getText().toString();
        SQLiteDatabase db = MyActivity.helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("student_id", strStudentID + " ");
        values.put("name", strName + " ");
        values.put("class", strClass + " ");
        values.put("phone", strPhoneNumber + " ");
        db.insert("contacts", null, values);
        /*MyActivity.helper.getReadableDatabase().execSQL("insert into news_table values(null, ?,?,?,?)",
                new String[]{strStudentID, strName, strClass, strPhoneNumber});
        ArrayList<String> str = new ArrayList<String>();
        str.add(strStudentID);
        str.add(strName);
        str.add(strClass);
        str.add(strPhoneNumber);
        Intent intent = new Intent(AddContact.this, MyActivity.class);
        intent.putStringArrayListExtra("str", str);*/
        Intent intent = new Intent(AddContact.this, MyActivity.class);
        startActivity(intent);
    }
}
