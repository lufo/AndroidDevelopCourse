package com.example.lufo.lab8;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;


public class MyActivity extends Activity {

    public static DataBaseHelper helper;
    public static SimpleCursorAdapter cursorAdapter;
    private Button mButton;
    public static ListView mListView;
    private Cursor cursor;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        helper = new DataBaseHelper(getApplicationContext(), "contact.db", 1);
        mButton = (Button) findViewById(R.id.button);
        mListView = (ListView) findViewById(R.id.listview);

        mButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MyActivity.this, AddContact.class);
                startActivity(intent);
            }
        });

        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MyActivity.this);
                builder.setMessage("delete?");
                builder.setPositiveButton("confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        cursor = helper.getReadableDatabase().query("contacts", null, null, null, null, null, null);
                        cursor.moveToPosition(position);
                        long id = cursor.getLong(0);
                        SQLiteDatabase db = helper.getWritableDatabase();
                        db.execSQL("delete from contacts where _id =" + id);
                        cursor = helper.getReadableDatabase().query("contacts", null, null, null, null, null, null);
                        cursorAdapter.changeCursor(cursor);
                        mListView.setAdapter(MyActivity.cursorAdapter);
                    }
                });
                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.create().show();
                return true;
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {
                cursor = helper.getReadableDatabase().query("contacts", null, null, null, null, null, null);
                cursor.moveToPosition(position);
                long id = cursor.getLong(0);
                MyDialog myDialog = new MyDialog(context, R.layout.dialog_my, id);
                myDialog.show();
                cursor = helper.getReadableDatabase().query("contacts", null, null, null, null, null, null);
                inflateListView(cursor);
            }
        });

        context = this;
        cursor = helper.getReadableDatabase().query("contacts", null, null, null, null, null, null);
        if (cursor.getCount() != 0) {
            //Log.v("666", cursor.getCount() + "");
            inflateListView(cursor);
        }
/*      context = this;
        try {
            Intent intent = this.getIntent();
            ArrayList<String> str = intent.getStringArrayListExtra("str");
            //helper.getReadableDatabase().execSQL("insert into contacts values(null,?,?,?,?);", str.toArray());
            //helper.getReadableDatabase().execSQL("insert into contacts (student_id,name,class,phone) values(\"fd\",\"d\",\"2\",\"4\");");
            Cursor cursor = helper.getReadableDatabase().rawQuery("select * from contacts", null);
            inflateListView(cursor);
        } catch (NullPointerException e) {
            System.out.println(e);
        }*/
    }

    public void inflateListView(Cursor cursor) {
        cursorAdapter = new SimpleCursorAdapter(
                context,
                R.layout.item,
                cursor,
                new String[]{"student_id", "name", "class", "phone"},
                new int[]{R.id.student_id_output, R.id.name_output, R.id.class_output, R.id.phone_number_output}, 0);

        mListView.setAdapter(cursorAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}