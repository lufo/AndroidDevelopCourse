package com.example.lufo.lab8;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by lufo on 14-11-29.
 */

public class DataBaseHelper extends SQLiteOpenHelper {
    final String SQL_CREATE_TABLE = "create table contacts (" +
            "_id integer primary key autoincrement,  " +
            "student_id varchar(10), " +
            "name varchar(10)," + "class varchar(5)," + "phone varchar(15));";

    /*
     * 构造方法 :
     * 参数介绍 :
     * 参数① : 上下文对象
     * 参数② : 数据库名称
     * 参数③ : 数据库版本号
     */
    public DataBaseHelper(Context context, String name, int version) {
        super(context, name, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
