package com.example.lufo.lab8;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by lufo on 14-11-29.
 */
public class MyDialog extends Dialog {
    private long id;
    private int layoutRes;//布局文件
    private Context context;
    private EditText studentID;
    private EditText name;
    private EditText mClass;
    private EditText phoneNumber;
    private Button update;
    private Button cancel;

    public MyDialog(Context context) {
        super(context);
        this.context = context;
    }

    /**
     * 自定义布局的构造方法
     *
     * @param context
     * @param resLayout
     */
    public MyDialog(Context context, int resLayout, long id) {
        super(context);
        this.id = id;
        this.context = context;
        this.layoutRes = resLayout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(layoutRes);
        studentID = (EditText) findViewById(R.id.student_id_input2);
        name = (EditText) findViewById(R.id.name_input2);
        mClass = (EditText) findViewById(R.id.class_input2);
        phoneNumber = (EditText) findViewById(R.id.phone_number_input2);
        update = (Button) findViewById(R.id.update);
        cancel = (Button) findViewById(R.id.cancel);

        update.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String strStudentID = studentID.getText().toString();
                String strName = name.getText().toString();
                String strClass = mClass.getText().toString();
                String strPhoneNumber = phoneNumber.getText().toString();
                SQLiteDatabase db = MyActivity.helper.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put("student_id", strStudentID + " ");
                values.put("name", strName + " ");
                values.put("class", strClass + " ");
                values.put("phone", strPhoneNumber + " ");
                String whereClause = "_id=?";
                String[] whereArgs = {String.valueOf(id)};
                db.update("contacts", values, whereClause, whereArgs);
                Cursor cursor = MyActivity.helper.getReadableDatabase().query("contacts", null, null, null, null, null, null);
                MyActivity.cursorAdapter.changeCursor(cursor);
                MyActivity.mListView.setAdapter(MyActivity.cursorAdapter);
                dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
