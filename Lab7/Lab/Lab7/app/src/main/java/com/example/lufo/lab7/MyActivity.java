package com.example.lufo.lab7;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;

public class MyActivity extends Activity {
    private static final String DATABASE = "Database";
    private static final String PATH = "/data/data/com.example.lufo.lab7/shared_prefs/Database.xml";
    private AutoCompleteTextView usernameInput;
    private EditText passwordInput;
    private Button insert;
    private Button delete;
    private Button find;
    private Button clear;
    private Button custom;
    private TextView findResult;
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private ArrayList<String> autoStr = new ArrayList<String>();
    private ArrayAdapter<String> autoAdapter;
    private StringBuffer buff;
    private BufferedReader reader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        usernameInput = (AutoCompleteTextView) findViewById(R.id.username_input);
        passwordInput = (EditText) findViewById(R.id.password_input);
        insert = (Button) findViewById(R.id.insert);
        delete = (Button) findViewById(R.id.delete);
        find = (Button) findViewById(R.id.find);
        clear = (Button) findViewById(R.id.clear);
        custom = (Button) findViewById(R.id.custom);
        findResult = (TextView) findViewById(R.id.find_result);
        mSharedPreferences = getSharedPreferences(DATABASE, Activity.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();

        Map<String, ?> mMap = mSharedPreferences.getAll();
        for (Map.Entry entry : mMap.entrySet())
            autoStr.add(entry.getKey().toString());
        autoAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, autoStr);
        usernameInput.setAdapter(autoAdapter);
        usernameInput.setThreshold(1);
        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditor.putString(usernameInput.getText().toString(), passwordInput.getText().toString());
                autoStr.add(usernameInput.getText().toString());
                mEditor.commit();
                autoAdapter = new ArrayAdapter<String>(MyActivity.this, android.R.layout.simple_dropdown_item_1line, autoStr);
                usernameInput.setAdapter(autoAdapter);
                usernameInput.setThreshold(1);
                String str;
                buff = new StringBuffer();
                try {
                    reader = new BufferedReader(new InputStreamReader(new FileInputStream(PATH)));
                    while ((str = reader.readLine()) != null)
                        buff.append(str);
                } catch (IOException e) {

                }
                findResult.setText(buff.toString());
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditor.remove(usernameInput.getText().toString());
                mEditor.commit();
                autoStr.remove(usernameInput.getText().toString());
                autoAdapter = new ArrayAdapter<String>(MyActivity.this, android.R.layout.simple_dropdown_item_1line, autoStr);
                usernameInput.setAdapter(autoAdapter);
                usernameInput.setThreshold(1);
                String str;
                buff = new StringBuffer();
                try {
                    Log.v("66", "666666");
                    reader = new BufferedReader(new InputStreamReader(new FileInputStream(PATH)));
                    while ((str = reader.readLine()) != null)
                        buff.append(str);
                } catch (IOException e) {
                    Log.v("7", "7777");
                }
                findResult.setText(buff.toString());
            }
        });
        find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String key = usernameInput.getText().toString();
                String value = mSharedPreferences.getString(key, "not_exist");
                findResult.setText(key + " " + value);
                //mEditor.commit();
            }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditor.clear();
                autoStr.clear();
                autoAdapter = new ArrayAdapter<String>(MyActivity.this, android.R.layout.simple_dropdown_item_1line, autoStr);
                usernameInput.setAdapter(autoAdapter);
                usernameInput.setThreshold(1);
                mEditor.commit();
                String str;
                buff = new StringBuffer();
                try {
                    reader = new BufferedReader(new InputStreamReader(new FileInputStream(PATH)));
                    while ((str = reader.readLine()) != null)
                        buff.append(str);
                } catch (IOException e) {

                }
                findResult.setText(buff.toString());
            }
        });
        custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                /* 开启Pictures画面Type设定为image */
                intent.setType("*/*");
                /* 使用Intent.ACTION_GET_CONTENT这个Action */
                intent.setAction(Intent.ACTION_GET_CONTENT);
                /* 取得相片后返回本画面 */
                startActivityForResult(intent, 1);
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Uri uri = data.getData();
            //Log.e("uri", uri.toString());
            //ContentResolver cr = this.getContentResolver();
            try {
                ContentResolver resolver = getContentResolver();
                InputStream is = resolver.openInputStream(uri);
                DataInputStream dis = new DataInputStream(is);
                byte[] buffer = new byte[is.available()];
                dis.readFully(buffer);
                findResult.setText(new String(buffer));
                dis.close();
                is.close();
                /*char c;
                String s = "";
                while ((c = (char) (is.read())) != -1) {
                    s += c;
                }
                findResult.setText(s);*/
            } catch (Exception e) {
                Log.v("66", "6666666");
            }
            /*
            try {
                String s = cr.openInputStream(uri).toString();
                //Bitmap bitmap = BitmapFactory.decodeStream(cr.openInputStream(uri));
                //ImageView imageView = (ImageView) findViewById(R.id.image);

                findResult.setText(s);
            } catch (FileNotFoundException e) {
                Log.e("Exception", e.getMessage(), e);
            }*/
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}