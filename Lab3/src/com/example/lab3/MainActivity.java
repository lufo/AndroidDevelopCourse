package com.example.lab3;

import com.example.lab3.R.id;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
	private Button button;
	private TextView text;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		button = (Button) findViewById(id.button);
		text = (TextView) findViewById(id.selected_frult);

		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(MainActivity.this, List.class);
				startActivity(intent);
			}
		});
		try {
			Bundle bundle = this.getIntent().getExtras();
			String fruit_name = bundle.getString("fruit_name");
			text.setText("you have chosen " + fruit_name);
		} catch (NullPointerException e) {
			System.out.println(e);
		}
	}
}
