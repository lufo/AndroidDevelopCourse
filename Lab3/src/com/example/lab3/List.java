package com.example.lab3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.example.lab3.R.id;

public class List extends Activity {
	private ArrayList<Map<String, Object>> myDataList = new ArrayList<Map<String, Object>>();

	SimpleAdapter listItemAdapter;

	ListView list;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list);
		setData();
		listItemAdapter = new SimpleAdapter(this, myDataList, R.layout.item,
				new String[] { "name", "image" }, new int[] { R.id.fruit_name,
						R.id.fruit_image });
		list = (ListView) findViewById(id.fruit_list);
		list.setAdapter(listItemAdapter);
		list.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				Bundle bundle = new Bundle();
				bundle.putString("fruit_name",
						myDataList.get(position).get("name").toString());
				Intent intent = new Intent();
				intent.setClass(List.this, MainActivity.class);
				intent.putExtras(bundle);
				startActivity(intent);
			}
		});
	}

	private void setData() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", "Apple");
		map.put("image", R.drawable.apple);
		myDataList.add(map);

		map = new HashMap<String, Object>();
		map.put("name", "Banana");
		map.put("image", R.drawable.banana);
		myDataList.add(map);

		map = new HashMap<String, Object>();
		map.put("name", "Cherry");
		map.put("image", R.drawable.cherry);
		myDataList.add(map);

		map = new HashMap<String, Object>();
		map.put("name", "Coco");
		map.put("image", R.drawable.coco);
		myDataList.add(map);

		map = new HashMap<String, Object>();
		map.put("name", "Kiwi");
		map.put("image", R.drawable.kiwi);
		myDataList.add(map);

		map = new HashMap<String, Object>();
		map.put("name", "Orange");
		map.put("image", R.drawable.orange);
		myDataList.add(map);

		map = new HashMap<String, Object>();
		map.put("name", "Pear");
		map.put("image", R.drawable.pear);
		myDataList.add(map);

		map = new HashMap<String, Object>();
		map.put("name", "Strawberry");
		map.put("image", R.drawable.strawberry);
		myDataList.add(map);

		map = new HashMap<String, Object>();
		map.put("name", "Watermelon");
		map.put("image", R.drawable.watermelon);
		myDataList.add(map);
	}
}
