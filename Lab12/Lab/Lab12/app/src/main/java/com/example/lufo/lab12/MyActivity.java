package com.example.lufo.lab12;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;


public class MyActivity extends Activity {
    MapView mMapView = null;
    BaiduMap mBaiduMap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //在使用SDK各组件之前初始化context信息，传入ApplicationContext
        //注意该方法要再setContentView方法之前实现
        SDKInitializer.initialize(getApplicationContext());
        setContentView(R.layout.activity_my);
        //获取地图控件引用
        mMapView = (MapView) findViewById(R.id.bmapView);
        mBaiduMap = mMapView.getMap();
        mBaiduMap.setMyLocationEnabled(true);
        mBaiduMap.setMapStatus(MapStatusUpdateFactory.zoomTo(18));
        double BDLat = 22.352921, BDLng = 113.596621;
        MyLocationData locationData = new MyLocationData.Builder().latitude(BDLat).longitude(BDLng).build();
        mBaiduMap.setMyLocationData(locationData);
        LatLng pt;
        pt = new LatLng(BDLat, BDLng);
        MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(pt);
        mBaiduMap.animateMapStatus(u);
        BitmapDescriptor mCurrentMarker = BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher);
        MyLocationConfiguration config = new MyLocationConfiguration(MyLocationConfiguration.LocationMode.NORMAL, true, mCurrentMarker);
        mBaiduMap.setMyLocationConfigeration(config);
        LatLng library = new LatLng(22.349821, 113.595543), shop = new LatLng(22.35618, 113.592003), bank = new LatLng(22.352821, 113.595615), stadium = new LatLng(22.355788, 113.587332);
        BitmapDescriptor bitmapLibrary = BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher);
        OverlayOptions optionsLibrary = new MarkerOptions()
                .position(library)
                .icon(bitmapLibrary)
                .zIndex(9)
                .draggable(true);
        final Marker markerLibrary = (Marker) (mBaiduMap.addOverlay(optionsLibrary));
        BitmapDescriptor bitmapShop = BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher);
        OverlayOptions optionsShop = new MarkerOptions()
                .position(shop)
                .icon(bitmapShop)
                .zIndex(9)
                .draggable(true);
        final Marker markerShop = (Marker) (mBaiduMap.addOverlay(optionsShop));
        BitmapDescriptor bitmapBank = BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher);
        OverlayOptions optionsBank = new MarkerOptions()
                .position(bank)
                .icon(bitmapBank)
                .zIndex(9)
                .draggable(true);
        final Marker markerBank = (Marker) (mBaiduMap.addOverlay(optionsBank));
        BitmapDescriptor bitmapStadium = BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher);
        OverlayOptions optionsStadium = new MarkerOptions()
                .position(stadium)
                .icon(bitmapStadium)
                .zIndex(9)
                .draggable(true);
        final Marker markerStadium = (Marker) (mBaiduMap.addOverlay(optionsStadium));

        mBaiduMap.setOnMarkerClickListener(new BaiduMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (marker == markerLibrary)
                    Toast.makeText(getApplicationContext(), "图书馆", Toast.LENGTH_SHORT).show();
                else if (marker == markerShop)
                    Toast.makeText(getApplicationContext(), "珠影超市", Toast.LENGTH_SHORT).show();
                else if (marker == markerBank)
                    Toast.makeText(getApplicationContext(), "中国银行", Toast.LENGTH_SHORT).show();
                else if (marker == markerStadium)
                    Toast.makeText(getApplicationContext(), "风雨球场", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getApplicationContext(), "未知地点", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理
        mMapView.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理
        mMapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理
        mMapView.onPause();
    }
}
