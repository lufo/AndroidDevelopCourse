<center>**实验报告**</center>

姓名：刘学博

学号：12353132

遇到的问题：

本次实验比较简单，遇到的一个问题是使用RelativeLayout时不知道怎么将TextView与EditText并排排列，在EditText加入android:layout_toRightOf="@id/textview1后解决该问题。

运行效果如下：

![image](lab1_1.png)

实验心得：

发现自己对Android的UI布局文件很不熟悉，对多种布局格式都不太了解，需要进一步学习写布局文件。