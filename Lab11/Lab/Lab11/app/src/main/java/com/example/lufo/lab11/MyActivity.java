package com.example.lufo.lab11;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;


public class MyActivity extends Activity {
    private TextView text;
    private Button getLatLng;
    private Button getLocation;
    private LocationManager mlocationManager;
    private Location location;
    private String address;
    private double lat = 0, lng = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        text = (TextView) findViewById(R.id.text);
        getLatLng = (Button) findViewById(R.id.lng_lat);
        getLocation = (Button) findViewById(R.id.location);
        mlocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        getLatLng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLatLng();
                text.setText("经度：" + Double.toString(lng) + "\n" + "纬度：" + Double.toString(lat));
            }
        });
        getLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLatLng();
                new Thread(runnable).start();
            }
        });
    }

    public void getLatLng() {
        final String provider = LocationManager.NETWORK_PROVIDER;
        location = mlocationManager.getLastKnownLocation(provider);
        if ((location != null)) {
            lat = location.getLatitude();
            lng = location.getLongitude();
        }
    }

    public android.os.Handler handler = new android.os.Handler() {
        @Override
        public void handleMessage(Message msg) {
            Bundle b = msg.getData();
            String result = b.get("result").toString();
            try {
                JSONObject jsonobj = new JSONObject(result);
                address = jsonobj.getJSONArray("results").getJSONObject(0).getString("formatted_address");
                text.setText(address);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    public Runnable runnable = new Runnable() {
        @Override
        public void run() {
            String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + Double.toString(lat) + "," + Double.toString(lng) + "&language=zh-CN&sensor=false";
            HttpClient client = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(url);
            StringBuilder mStringBulider = new StringBuilder();
            try {
                HttpResponse response = client.execute(httpget);
                HttpEntity entity = response.getEntity();
                InputStream stream = entity.getContent();
                int len = 0;
                byte[] b = new byte[1024];
                while ((len = stream.read(b, 0, b.length)) != -1) {
                    mStringBulider.append(new String(b, 0, len));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bundle b = new Bundle();
            b.putString("result", mStringBulider.toString());
            Message msg = handler.obtainMessage();
            msg.setData(b);
            handler.sendMessage(msg);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
