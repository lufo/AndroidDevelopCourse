package com.example.lab2;

import com.example.lab2.R.drawable;
import com.example.lab2.R.id;

import android.R.layout;
import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.os.Build;

public class MainActivity extends Activity {

	private EditText username;
	private EditText password;
	private Button resetButton;
	private ImageButton imgButton;
	private TextView hintText;
	private LinearLayout newLinearLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		username = (EditText) this.findViewById(R.id.username);
		password = (EditText) this.findViewById(R.id.password);
		resetButton = (Button) this.findViewById(R.id.reset);
		imgButton = (ImageButton) this.findViewById(R.id.imageButton);
		hintText = (TextView) this.findViewById(R.id.hint);
		newLinearLayout = (LinearLayout) this.findViewById(R.id.addedLayout);

		imgButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TextView addText = new TextView(MainActivity.this);
				if (username.getText().toString().equals("android")
						&& password.getText().toString().equals("2014")) {
					imgButton.setImageDrawable(getResources().getDrawable(
							R.drawable.images));
					// addText.setText("bingo!");
					hintText.setText("bingo!");
					username.setVisibility(View.INVISIBLE);
					password.setVisibility(View.INVISIBLE);
				} else {
					imgButton.setImageDrawable(getResources().getDrawable(
							R.drawable.ic_launcher));
					// addText.setText("wrong!");
					hintText.setText("wrong!");
					username.setText("");
					password.setText("");
				}
				// newLinearLayout.addView(addText);
			}
		});

		imgButton.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				TextView addText = new TextView(MainActivity.this);
				addText.setText("��̬���Ӱ�ť");
				newLinearLayout.addView(addText);
				return false;
			}
		});

		resetButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				username.setVisibility(View.VISIBLE);
				password.setVisibility(View.VISIBLE);
				username.setText("");
				password.setText("");
				imgButton.setImageDrawable(getResources().getDrawable(
						R.drawable.ic_launcher));
				hintText.setText("");
				newLinearLayout.removeAllViews();
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
