package com.example.lufo.lab10;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Message;
import android.util.Base64;
import android.util.Log;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by lufo on 14-12-11.
 */
class DownloadThread implements Runnable {

    private String code;
    private static Bitmap bitmap;
    private final String NAMESPACE = "http://WebXml.com.cn/";
    private final String METHODNAME = "enValidateByte";
    private final String SOAPACTION = "http://WebXml.com.cn/enValidateByte";
    private final String URL = "http://webservice.webxml.com.cn/WebServices/ValidateCodeWebService.asmx";

    public DownloadThread(String input) {
        code = input;
    }

    @Override
    public void run() {
        SoapObject request = new SoapObject(NAMESPACE, METHODNAME);
        request.addProperty("byString", code);
        Log.v("6666", code);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER10);
        //envelope.bodyOut = request;
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        HttpTransportSE transportSE = new HttpTransportSE(URL);

        try {
            transportSE.call(SOAPACTION, envelope);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        //Object result;
        SoapObject result = (SoapObject) envelope.bodyIn;
        try {
            SoapPrimitive detail = (SoapPrimitive) result.getProperty("enValidateByteResult");
            byte[] data = Base64.decode(detail.toString(), Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        /*
        try {
            result = (Object) envelope.getResponse();
            //SoapPrimitive detail = (SoapPrimitive) ((SoapObject) result).getProperty("enValidateByteResult");
            byte[] data = Base64.decode(((SoapObject) result).getProperty("enValidateByteResult").toString(), Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
        } catch (SoapFault e) {
            e.printStackTrace();
        }
        */

        Bundle b = new Bundle();
        b.putString("state", "OK");
        Message msg = MyActivity.handler.obtainMessage();
        msg.setData(b);
        MyActivity.handler.sendMessage(msg);
    }

    public static Bitmap returnBitmap() {
        return bitmap;
    }
}